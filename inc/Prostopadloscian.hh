#ifndef PROSTOPADLOSCIAN_HH
#define PROSTOPADLOSCIAN_HH
#include "BrylaGeometryczna.hh"
#include <iostream>

class Prostopadloscian : public BrylaGeometryczna
{

public:
Prostopadloscian(double szerokosc, double dlugosc, double wysokosc)
{
   Wierzcholki.resize(8);
  
    for (int i = 0; i < 8; i++) 
    {
        Wierzcholki[i][0] -= szerokosc/2;
                Wierzcholki[i][1] -= dlugosc/2;
                        Wierzcholki[i][2] -= wysokosc/2;
    }

    Wierzcholki[1][0] += szerokosc;

    Wierzcholki[2][1] += dlugosc;

    Wierzcholki[3][0] += szerokosc;
    Wierzcholki[3][1] += dlugosc;

    Wierzcholki[4][1] += dlugosc;
    Wierzcholki[4][2] += wysokosc;

    Wierzcholki[5][0] += szerokosc;
    Wierzcholki[5][1] += dlugosc;
    Wierzcholki[5][2] += wysokosc;

    Wierzcholki[6][2] += wysokosc; 

    Wierzcholki[7][0] += szerokosc;
    Wierzcholki[7][2] += wysokosc;

}

};


std::ostream &operator<<(std::ostream &Strm, const Prostopadloscian &Prost)
{
  for (int i = 0; i < Prost.rozmiar(); i++)
  {
  if(i%2==0){Strm<<std::endl;}
    Strm << Prost[i] << std::endl;
  }
  Strm<<std::endl;
      Strm << Prost[0] << std::endl;
          Strm << Prost[1] << std::endl;
  return Strm;
}

#endif