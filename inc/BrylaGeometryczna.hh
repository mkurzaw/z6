#ifndef BRYLAGEO_HH
#define BRYLAGEO_HH
#include "Wektor3D.h"
#include "Macierz3x3.hh"
#include <vector>



class BrylaGeometryczna
{

protected:
    std::vector<Wektor3D> Wierzcholki;
    std::string nazwa;
        std::string nazwa_zap;
    Wektor3D Wekt_prze;
    Macierz3x3 Mobr;
    double kat;

public:
Wektor3D get_prze(){return Wekt_prze;}


    int rozmiar() const { return (int)Wierzcholki.size(); }

    void set_nazwa(std::string nazwa) { this->nazwa = nazwa; }
   

    void set_nazwa_zap(std::string nazwa) { this->nazwa_zap = nazwa; }
      std::string get_nazwa_zap() { return this->nazwa_zap ;} 

    void przesun(Wektor3D przesuniecie, double kat_wznoszenia, double Kat_Obrotu)
    {
        kat += Kat_Obrotu; 
        os_Z(Mobr, kat);  
        std::ifstream pliki; 
        Macierz3x3 Mwznoszenia;
        os_Y(Mwznoszenia, kat_wznoszenia);
        pliki.open(nazwa);
        for (int i = 0; i < (int)Wierzcholki.size(); i++)
            pliki >> Wierzcholki[i];
        pliki.close();

        Wekt_prze = Wekt_prze + (Mwznoszenia * (Mobr * przesuniecie)); 
        for (int i = 0; i < (int)Wierzcholki.size(); i++)
        {
            Wierzcholki[i] = (Mobr * Wierzcholki[i]) + Wekt_prze; 
        }
    }

    Wektor3D operator[](int i) const { return Wierzcholki[i]; } 

    Wektor3D &operator[](int i) { return Wierzcholki[i]; } 
};

#endif