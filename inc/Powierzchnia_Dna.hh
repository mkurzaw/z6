#include "BrylaGeometryczna.hh"


class Powierzchnia_Dna : public BrylaGeometryczna
{
    int siatka;

public:
    Wektor3D wek;
 
    int return_siatka() const { return siatka; }
    Powierzchnia_Dna(int grid, double rozpietosc, double powie)
    {
        siatka = 0;
        for (int i = -rozpietosc / 2; i <= rozpietosc / 2; i += grid)
        {
            siatka++;
            for (int j = -rozpietosc / 2; j <= rozpietosc / 2; j += grid)
            {
                wek[0] = i;
                wek[1] = j;
                wek[2] = powie;

                Wierzcholki.push_back(wek);
            }
        }
    }
};


std::ostream &operator<<(std::ostream &Strm, const Powierzchnia_Dna &pow)
{
    for (int i = 0; i < pow.rozmiar(); i++)
    {

        if (i % pow.return_siatka() == 0)
        {
            Strm << std::endl;
        }
        Strm << pow[i] << std::endl;
    }
    return Strm;
}