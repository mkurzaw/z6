#ifndef SMacierz_HH
#define SMacierz_HH


#include "Wektor.hh"
#include <iostream>

/*! 
 * \file Plik zawiera definicje szablonu SMacierz
 * \brief Szablon modeluje pojęcie Macierzy
 * Pola:
 * \brief M[ROZMIAR] - Tablice wektorów modulujące macierz
 * \brief Wyznacznik - Pole przechowujące wyznacznik; 
 */

template <class Typ, int ROZMIAR>
class SMacierz
{
  SWektor<Typ, ROZMIAR> M[ROZMIAR];
  Typ Wyznacznik;

public:
  /*!
  * \brief Szablon przeciążenia pozwalającego nam na zmiane wartosci wiersza
  * \param[in] i - indeks wiersza macierzy
  * \return wektor o podanym indeksie
  */
  SWektor<Typ, ROZMIAR> &operator[](int i)
  {
    return M[i];
  }

  /*!
  * \brief Szablon przeciążenia pozwalającego nam na zwrócenie const oryginału wartosci wiersza
  * \param[in] i - indeks wiersza macierzy
  * \return wektor o podanym indeksie
  */
  SWektor<Typ, ROZMIAR> operator[](int i) const 
  {
    return M[i];
  }

  /*!
  * \brief Metoda pozwalająca na zwrócenie wartości wyznacznika
  */
  Typ get_wyznacznik()
  {
    return Wyznacznik;
  }
  void Oblicz_Wyznacznik();

  SWektor<Typ, ROZMIAR> operator*(SWektor<Typ, ROZMIAR> W);
};

/*!
* \brief Przeciążenie realizujące wczytywanie danych
* Argumenty:
* \param[in] Strm - Argument strumienia wejściowego
* \param[in] Mac - Argument Macierzy
* Zwraca:
* \return Oryginalny obiekt strumienia wejściowego
*/
template <class Typ, int ROZMIAR>
std::istream &operator>>(std::istream &Strm, SMacierz<Typ, ROZMIAR> &Mac)
{
  for (int i = 0; i < ROZMIAR; i++)
    Strm >> Mac[i];
  return Strm;
}

/*!
* \brief Przeciążenie realizujące wypisywanie danych
* Argumenty:
* \param[in] Strm - Argument strumienia wyjściowego
* \param[in] Mac - Argument Macierzy
* Zwraca:
* \return Oryginalny obiekt strumienia wyjściowego
*/
template <class Typ, int ROZMIAR>
std::ostream &operator<<(std::ostream &Strm, const SMacierz<Typ, ROZMIAR> &Mac)
{
  for (int i = 0; i < ROZMIAR; i++)
    Strm << Mac[i] << std::endl;
  return Strm;
}

/*!
* \brief Przeciążenie realizujace iloczyn Macierzy razy wektor
* Argumenty:
* \param[in] W - Argument Wektra służący jako składnik mnożenia
* Zwraca:
* \return wynik - wynik ilocznu macierzy przez wektor
*/
template <class Typ, int ROZMIAR>
SWektor<Typ, ROZMIAR> SMacierz<Typ, ROZMIAR>::operator*(SWektor<Typ, ROZMIAR> W)
{
  SWektor<Typ, ROZMIAR> wynik;

  for (int i = 0; i < ROZMIAR; i++)
  {
    wynik[i] = {0};
    for (int j = 0; j < ROZMIAR; j++)
      wynik[i] = wynik[i] + M[j][i] * W[j];
  }
  return wynik;
}

/*!
* \brief Funkcja realizująca liczenie wyznacznika metodą Gaussa
* Argumenty:
* \param[in] - Brak
* Zwraca:
* \return Wartość obliczonego wyznacznika do pola klasy
*/
template <class Typ, int ROZMIAR>
void SMacierz<Typ, ROZMIAR>::Oblicz_Wyznacznik()
{
  SMacierz<Typ, ROZMIAR> tmp = *this; 
  Typ skalar;
  SWektor<Typ, ROZMIAR> tmp_Wek;
  Wyznacznik = {1};
  int k;
  Typ m = {1};
  for (int i = 0; i < ROZMIAR - 1; i++) 
  {
    k = i;
    do
    {
      k++;            
      if (k >= ROZMIAR) 
      {
        Wyznacznik = {0}; 
        return;
      }
      else if (tmp[i][i] == 0) 
      {
        m = m * -1;      
        tmp_Wek = tmp[i]; 
        tmp[i] = tmp[k];  
        tmp[k] = tmp_Wek; 
      }

    } while (tmp[i][i] == 0);

    tmp_Wek = tmp[i];                 
    for (int j = 1 + i; j < ROZMIAR; j++)
    {
      skalar = tmp[j][i] / tmp[i][i];    
      tmp[j] = tmp[j] - (tmp_Wek * skalar); 
    }
  }

  for (int i = 0; i < ROZMIAR; i++)      
    Wyznacznik = Wyznacznik * tmp[i][i]; 

  Wyznacznik = Wyznacznik * m; 
}

#endif
