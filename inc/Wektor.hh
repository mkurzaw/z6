#ifndef Wektor_HH
#define Wektor_HH


#include <iostream>

/*!
 * \file Plik zawiera definicje szablonu SWektor
 * \brief tab[ROZMIAR] - Tablice wartości modelujące wektor
 * 
 */

template <class Typ, int ROZMIAR>
class SWektor
{
  static int _AktualnaIlosc;
  static int _LacznaIlosc;
  Typ tab[ROZMIAR];

public:
  /*!
  * \brief Szablon przeciążenia pozwalającego nam na zmiane wartosci elementu
  * \param[in] i - indeks elementu wektora
  * \return pole o podanym indeksie
  */
  Typ &operator[](int i) 
  {
    return tab[i];
  }

  /*!
  * \brief Szablon przeciążenia pozwalającego nam na zwrócenie const oryginału elementu
  * \param[in] i - indeks elementu wektora
  * \return wartosc o podanym indeksie
  */
  Typ operator[](int i) const
  {
    return tab[i];
  }

  SWektor<Typ, ROZMIAR> operator+(SWektor<Typ, ROZMIAR> skladnik);
  SWektor<Typ, ROZMIAR> operator-(SWektor<Typ, ROZMIAR> odjemnik);
  Typ operator*(SWektor<Typ, ROZMIAR> mnoznik);
  SWektor<Typ, ROZMIAR> operator*(Typ mnoznik);
  SWektor<Typ, ROZMIAR> operator/(Typ dzielnik);

  SWektor() {  
    for(int i=0; i<ROZMIAR; i++)
      tab[i]=0;
    ++_AktualnaIlosc; ++_LacznaIlosc;}
  SWektor(const SWektor<Typ,ROZMIAR> & wek) {
    for(int i=0; i<ROZMIAR; i++)
    tab[i]=wek[i];

    ++_AktualnaIlosc; ++_LacznaIlosc;}

  ~SWektor() {--_AktualnaIlosc; }

  static int WezAktualnaIlosc() {return _AktualnaIlosc;}
  static int WezLacznaIlosc() {return _LacznaIlosc;}
};

template <class Typ, int ROZMIAR>
int SWektor<Typ,ROZMIAR>::_AktualnaIlosc = 0;

template <class Typ, int ROZMIAR>
int SWektor<Typ,ROZMIAR>::_LacznaIlosc = 0;

/*!
* \brief Przeciążenie realizujące wczytanie danych na standardowy strumień wejsciowy element po elemencie
* Argumenty:
* \param[in] Strm - Argument strumienia wejściowego
* \param[in] Wek -  Argument Wektor 
* Zwraca:
* \return Oryginalny obiekt strumienia wejściowego
*/
template <class Typ, int ROZMIAR>
std::istream &operator>>(std::istream &Strm, SWektor<Typ, ROZMIAR> &Wek)
{
  for (int i = 0; i < ROZMIAR; i++)
    Strm >> Wek[i];
  return Strm;
}

/*!
* \brief Przeciążenie realizujace sumowanie dwóch obiektów typu wektor
* Argumenty:
* \param[in] skladnik - Argument Wektor jako jeden ze współczynników dodawania
* Zwraca:
* \return Wynik - wynik dodawania dwóch obiektów typu wektor
*/
template <class Typ, int ROZMIAR>
SWektor<Typ, ROZMIAR> SWektor<Typ, ROZMIAR>::operator+(SWektor<Typ, ROZMIAR> skladnik)
{
  SWektor<Typ, ROZMIAR> wynik;
  for (int i = 0; i < ROZMIAR; i++)
    wynik[i] = tab[i] + skladnik[i];
  return wynik;
}

/*! 
* \brief Przeciążenie realizujące różnice dwóch obiektów typu wektor
* Argumenty:
* \param[in] odjemnik - Argument Wektor jako jeden ze współcznników odejmowania
* Zwraca:
* \return Wynik - wynik odejmowania dwóch obiektów typu wektor
*/
template <class Typ, int ROZMIAR>
SWektor<Typ, ROZMIAR> SWektor<Typ, ROZMIAR>::operator-(SWektor<Typ, ROZMIAR> odjemnik)
{
  SWektor<Typ, ROZMIAR> wynik;
  for (int i = 0; i < ROZMIAR; i++)
    wynik[i] = tab[i] - odjemnik[i];
  return wynik;
}

/*!
* \brief Przeciążenie realizujące iloczyn Wektora przez Skalar
* Argumenty:
* \param[in] mnoznik - Argument służący jako skalar 
* Zwraca:
* \return Wynik - wynik iloczynu wektora przez skalar
*/
template <class Typ, int ROZMIAR>
SWektor<Typ, ROZMIAR> SWektor<Typ, ROZMIAR>::operator*(Typ mnoznik) 
{
  SWektor<Typ, ROZMIAR> wynik;
  for (int i = 0; i < ROZMIAR; i++)
    wynik[i] = tab[i] * mnoznik;
  return wynik;
}

/*!
* \brief Przeciążenie realizujące iloczyn Wektora razy Wektor
* Argumenty:
* \param[in] mnoznik - Argument służący jako mnożnik 
* Zwraca:
* \return Wynik - wynik to wartosc sklarna                          
*/
template <class Typ, int ROZMIAR>
Typ SWektor<Typ, ROZMIAR>::operator*(SWektor<Typ, ROZMIAR> mnoznik)
{
  Typ wynik = {0};
  for (int i = 0; i < ROZMIAR; i++)
    wynik = wynik + tab[i] * mnoznik[i];
  return wynik;
}

/*!
* \brief Przeciążenie realizujące iloraz Wektora przez Skalar
* Argumenty:
* \param[in] dzielnik - Argument służący jako dzielnik 
* Zwraca:
* \return Wynik - wynik ilorazu wektora przez skalar
*/
template <class Typ, int ROZMIAR>
SWektor<Typ, ROZMIAR> SWektor<Typ, ROZMIAR>::operator/(Typ dzielnik)
{
  SWektor<Typ, ROZMIAR> wynik;
  for (int i = 0; i < ROZMIAR; i++)
    wynik[i] = tab[i] / dzielnik;
  return wynik;
}

/*!
* \brief Przeciążenie realizujące wypisanie danych na standardowy strumień wyjściowy element po elemencie
* Argumenty:
* \param[in] Strm - Argument strumienia wyjściowego
* \param[in] Wek  - Argument Wektor 
* Zwraca:
* \return oryginalny obiekt strumienia wyjściowego
*/
template <class Typ, int ROZMIAR>
std::ostream &operator<<(std::ostream &Strm, const SWektor<Typ, ROZMIAR> &Wek)
{
  for (int i = 0; i < ROZMIAR; i++)
    Strm << Wek[i] << "\t";
  return Strm;
}
#endif
