#ifndef GRANIASTOSLUP_HH
#define GRANIASTOSLUP_HH
#include "BrylaGeometryczna.hh"
#include <iostream>

class Graniastoslup : public BrylaGeometryczna
{
double kat_rotacji;
public:

Graniastoslup(Wektor3D srodek, double promien)
{
  Wektor3D tmp;
  kat=0;
  kat_rotacji=0;
    for (int i = 0; i >=-300; i-=60) 
    {
		tmp[2] = srodek[2] + promien*cos(i*M_PI/180);
		tmp[1] = srodek[1] + promien*sin(i*M_PI/180);
		tmp[0] = srodek[0] + 5;
		Wierzcholki.push_back(tmp);

		tmp[2] = srodek[2] + promien*cos(i*M_PI/180);
		tmp[1] = srodek[1] + promien*sin(i*M_PI/180);
		tmp[0] = srodek[0];
		Wierzcholki.push_back(tmp);
	}

	tmp[2] = srodek[2];
	tmp[1] = srodek[1];
	tmp[0] = srodek[0] + 5;
	Wierzcholki.push_back(tmp);

	tmp[2] = srodek[2];
	tmp[1] = srodek[1];
	tmp[0] = srodek[0];
	Wierzcholki.push_back(tmp);

}


void ustaw(Wektor3D srodek, double promien,double Kat_Obrotu)
{

    Wierzcholki.clear();
	Macierz3x3 rotacji;
      kat += Kat_Obrotu;   
	  kat_rotacji+=5;
	  os_X(rotacji,kat_rotacji);
        os_Z(Mobr, kat);    
   Wektor3D tmp;
  
    for (int i = 0; i >=-300; i-=60) 
    {
		tmp[2] = srodek[2] + promien*cos(i*M_PI/180);
		tmp[1] = srodek[1] + promien*sin(i*M_PI/180);
		tmp[0] = srodek[0] + 5;
		Wierzcholki.push_back(Mobr *(rotacji*(tmp-srodek))+srodek);

		tmp[2] = srodek[2] + promien*cos(i*M_PI/180);
		tmp[1] = srodek[1] + promien*sin(i*M_PI/180);
		tmp[0] = srodek[0];
		Wierzcholki.push_back(Mobr *(rotacji*(tmp-srodek))+srodek);
	}

	tmp[2] = srodek[2];
	tmp[1] = srodek[1];
	tmp[0] = srodek[0] + 5;
		Wierzcholki.push_back(Mobr *(rotacji*(tmp-srodek))+srodek);

	tmp[2] = srodek[2];
	tmp[1] = srodek[1];
	tmp[0] = srodek[0];
		Wierzcholki.push_back(Mobr *(rotacji*(tmp-srodek))+srodek);

}

};


std::ostream& operator << (std::ostream &Strm, const Graniastoslup &Gr)
{
    for(int i=0;i<(int)Gr.rozmiar()-2;i+=2)
    {
        Strm << Gr[12] << std::endl<< Gr[i] << std::endl<< Gr[i+1] << std::endl<< Gr[13]<< std::endl<<std::endl;
    }

	Strm << Gr[12] << std::endl<< Gr[0] << std::endl<< Gr[1] << std::endl<< Gr[13]; 

    return Strm;
}

#endif