#ifndef Macierz3x3_HH
#define Macierz3x3_HH
#include "Macierz.hh"
#include <cmath>


typedef SMacierz<double, 3> Macierz3x3;


void os_Z(Macierz3x3 &Mobr, double kat)
{
    kat = kat * M_PI / 180;
    Mobr[0][0] = cos(kat);
    Mobr[0][1] = -sin(kat);
    Mobr[0][2] = 0;
    Mobr[1][0] = sin(kat);
    Mobr[1][1] = cos(kat);
    Mobr[1][2] = 0;
    Mobr[2][0] = 0;
    Mobr[2][1] = 0;
    Mobr[2][2] = 1;
}



void os_Y(Macierz3x3 &Mwznoszenia, double kat_wznoszenia)
{
    kat_wznoszenia = kat_wznoszenia * M_PI / 180;
    Mwznoszenia[0][0] = cos(kat_wznoszenia);
    Mwznoszenia[0][1] = 0;
    Mwznoszenia[0][2] = sin(kat_wznoszenia);
    Mwznoszenia[1][0] = 0;
    Mwznoszenia[1][1] = 1;
    Mwznoszenia[1][2] = 0;
    Mwznoszenia[2][0] = -sin(kat_wznoszenia);
    Mwznoszenia[2][1] = 0;
    Mwznoszenia[2][2] = cos(kat_wznoszenia);
}


void os_X(Macierz3x3 &Mwznoszenia, double kat_wznoszenia)
{
    kat_wznoszenia = kat_wznoszenia * M_PI / 180;
    Mwznoszenia[0][0] = 1;
    Mwznoszenia[0][1] = 0;
    Mwznoszenia[0][2] = 0;
    Mwznoszenia[1][0] = 0;
    Mwznoszenia[1][1] = cos(kat_wznoszenia);
    Mwznoszenia[1][2] = -sin(kat_wznoszenia);
    Mwznoszenia[2][0] = 0;
    Mwznoszenia[2][1] = sin(kat_wznoszenia);
    Mwznoszenia[2][2] = cos(kat_wznoszenia);
}

#endif