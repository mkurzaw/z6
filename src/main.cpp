#include <iostream>
#include <fstream>
#include <iomanip>
#include <memory>
#include "lacze_do_gnuplota.hh"
#include <string>
#include <cassert>
#include <unistd.h>
#include "Wektor3D.h"
#include "Prostopadloscian.hh"
#include "Powierzchnia.hh"
#include "Powierzchnia_Dna.hh"
#include "Graniastoslup.hh"
#include "PPlaszczyzna.hh"
#include "PPret.hh"
#include "PProstopadloscienna.hh"
#include <memory>
#include <list>
using namespace std;

/*!
* \file Plik zawiera funkcję main
*/
int main()
{
  cout<<"r - zadaj ruch na wprost"<<endl;
  cout<<"o - zadaj zmiane orientacji"<<endl;
  cout<<"m - wyswietl menu"<<endl;
  cout<<"k - koniec dzialania programu"<<endl;
  cout<<"u - usun przeszkode"<<endl;
  cout<<"d - dodaj przeszkkode"<<endl;
  cout<<"Aktualna ilosc obiektow Wektor3D: 313"<<endl;
  cout<<"Laczna ilosc obiektow Wektor3D: 1528"<<endl;
  cout<<"Twoj wybor, m - menu> "<<endl;
  PzG::LaczeDoGNUPlota Lacze;
 
  list< shared_ptr< Przeszkoda  > > przeszkody;
  przeszkody.push_back(make_shared< PPret>(0,0,30));
   przeszkody.push_back(make_shared< PPlaszczyzna>(0,10,10));
    przeszkody.push_back(make_shared< PProstopadloscienna>(10,10,10));
  Wektor3D w;
  ofstream pliki;
  int m=0;
  for(list<shared_ptr< Przeszkoda  > >::iterator i=przeszkody.begin(); i!=przeszkody.end(); i++)
{
    w[0] = 0;
  w[1] = 0;
  w[2] = 0;
  w[0] = 0;
  w[1] = -20;
  w[2] = 10;

  (*i)->przesun(w, 0, 0); 
   (*i)->set_nazwa("bryly/orgprzesz"+to_string(m)+".dat"); 
    (*i)->set_nazwa_zap("bryly/przesz"+to_string(m)+".dat"); 
  pliki.open("bryly/przesz"+to_string(m)+".dat");
  pliki << *(*i);
  pliki.close();
  Lacze.DodajNazwePliku((*i)->get_nazwa_zap().c_str());
  m++;
}
  
/*!
* \brief pros - Określamy rozmiar drona
* \brief pow  - Określamy rozmiar powierzchnii górnej
* \brief dno  - Określamy rozmiar dna
*/
  Prostopadloscian pros(10, 10, 10);
  Powierzchnia pow(10, 60, 50, 10);
  Powierzchnia_Dna dno(10, 60, -10);
  Graniastoslup smiglo1(pros[0],5);
  Graniastoslup smiglo2(pros[2],5);
/*!
* \brief Zapisujemy określone początkowe wartości do określonych plików
*/ 

  pros.set_nazwa("bryly/orginal.dat"); 
  pliki.open("bryly/fale.dat");
  pliki << pow;
  pliki.close();

  pliki.open("bryly/dno.dat");
  pliki << dno;
  pliki.close();
  pliki.open("bryly/pros.dat");
  pliki << pros;
  pliki.close();
  pliki.open("bryly/orginal.dat");
  pliki << pros;
  pliki.close();



 

  smiglo1.set_nazwa("bryly/orginals1.dat"); 
  pliki.open("bryly/s1.dat");
  pliki << smiglo1;
  pliki.close();
  pliki.open("bryly/orginals1.dat");
  pliki << smiglo1;
  pliki.close();


  smiglo2.set_nazwa("bryly/orginals2.dat"); 
  pliki.open("bryly/s2.dat");
  pliki << smiglo2;
  pliki.close();
  pliki.open("bryly/orginals2.dat");
  pliki << smiglo2;
  pliki.close();
/*!
* \brief Łącze z Gnuplota zczytuje pliki
*/ 
  
 
  Lacze.DodajNazwePliku("bryly/pros.dat");
  Lacze.DodajNazwePliku("bryly/dno.dat");
  Lacze.DodajNazwePliku("bryly/pros.dat");
  Lacze.DodajNazwePliku("bryly/dno.dat");
  Lacze.DodajNazwePliku("bryly/fale.dat");
  Lacze.DodajNazwePliku("bryly/s1.dat");
  Lacze.DodajNazwePliku("bryly/s2.dat");
  Lacze.ZmienTrybRys(PzG::TR_3D);
  Lacze.Inicjalizuj(); // Tutaj startuje gnuplot.

  Lacze.UstawZakresX(-40, 100);
  Lacze.UstawZakresY(-90, 90);
  Lacze.UstawZakresZ(-20, 90);

  Lacze.UstawRotacjeXZ(40, 60); // Tutaj ustawiany jest widok
  char opcja;
  int wartosc;
  int koniec=0;
  double kat_wznoszenia;
  Lacze.Rysuj();
  /*!
  * \brief Opcje - r(ruch), o(obrót), m(menu), k(koniec)
  */
int nr;
  while (koniec==0)
  {
    cin >> opcja;
    switch (opcja)
    {

      case 'd':
      {
        char wybor;
          cout<<"1-prostopadloscian \n 2-plaszczyzna \n 3-pret "<<endl;
            cin>>wybor;
            switch (wybor)
            {
            case '1':
              przeszkody.push_back(make_shared<PProstopadloscienna>(12,14,12));
    
              break;
                       case '2':
              przeszkody.push_back(make_shared<PPlaszczyzna>(20,10,0));
             
              break; 
                                     case '3':
              przeszkody.push_back(make_shared<PPret>(12,0,0));
           
              break; 
            default:
              break;
            }

    w[0] = 0;
  w[1] = 0;
  w[2] = 0;
  w[0] = rand()%30+30;
  w[1] = rand()%30+30;
  

  (*(--przeszkody.end()))->przesun(w, 0, 0); 
    (*(--przeszkody.end()))->set_nazwa("bryly/orgprzesz"+to_string(m)+".dat"); 
     (*(--przeszkody.end()))->set_nazwa_zap("bryly/przesz"+to_string(m)+".dat"); 
  pliki.open("bryly/przesz"+to_string(m)+".dat");
  pliki << * (*(--przeszkody.end()));
  pliki.close();
  Lacze.DodajNazwePliku( (*(--przeszkody.end()))->get_nazwa_zap().c_str());
  m++;
Lacze.Rysuj();
      }
      break;
             case 'u':
           {
          list<shared_ptr< Przeszkoda  > >::iterator  i=przeszkody.begin();;
           cout<<"podaj numer przeszkody"<<endl;
            cin>>nr;
            
            for(int j=0; j<nr; j++)
           i++;
             

                  Lacze.UsunNazwePliku((*i)->get_nazwa_zap());
                  przeszkody.erase(i);
              Lacze.Rysuj();
           break;
           }
    case 'r':
      cout<<"Twoj wybor, m - menu> r"<<endl;
      cout<<"Podaj wartosc odleglosci, na ktora ma sie przemiescic dron."<<endl;
      cout<<"Wartosc odleglosci> "<<endl;
      cin >> wartosc; // podajemy wartość ruchu o ile ma się przesunąć do przodu
      cout<<"Podaj wartosc kata (wznoszenia/opadania) w stopniach."<<endl;
      cout<<"Wartosc kata> "<<endl;
      cin >> kat_wznoszenia; // podajemy wartość kątu wznoszenia (ujemna wartość -> opadanie, dodatnia -> wznoszenie, 0 -> brak zmian)
      for (int i = 0; i < wartosc ; i++)
      {

        w[0] = 1; // X-owa wektora ustawiona na 1 -> będzie się ruszał o kroku 1 (Gdy r=20 wtedy wykona 20 podjedyńczych ruchów o 1)
        pros.przesun(w, kat_wznoszenia, 0); 
        
        smiglo1.ustaw(pros[0],5,0); //dolny prawy
        smiglo2.ustaw(pros[2],-5,0); //dolny lewy
        pliki.open("bryly/pros.dat"); // zapis wartości po zmianach
        pliki << pros;
        pliki.close();
        pliki.open("bryly/s2.dat");
        pliki << smiglo2;
        pliki.close();

        pliki.open("bryly/s1.dat");
        pliki << smiglo1;
        pliki.close();
        usleep(200000); // przy sleep 200 ms płynny ruch jakby faktycznie się ruszał/pływał
        Lacze.Rysuj();
      }
      cout<<"Aktualna ilosc obiektow Wektor3D: " << Wektor3D::WezAktualnaIlosc() <<endl;
      cout<<"Laczna ilosc obiektow Wektor3D: " << Wektor3D::WezLacznaIlosc() <<endl;
      break;
    case 'o':
      cout<<"Twoj wybor, m - menu> o"<<endl;
      cout<<"Podaj wartosc kata obrotu w stopniach."<<endl;
      cout<<"Wartosc kata> "<<endl;
      cin >> wartosc;
      for (int i = 0; i < wartosc; i++)
      {
        w[0] = 0;
        pros.przesun(w, 0, 1);
        smiglo1.ustaw(pros[0],5,1);
        smiglo2.ustaw(pros[2],5,1);
    
        pliki.open("bryly/pros.dat");
        pliki << pros;
        pliki.close();

        pliki.open("bryly/s2.dat");
        pliki << smiglo2;
        pliki.close();

        pliki.open("bryly/s1.dat");
        pliki << smiglo1;
        pliki.close();
        usleep(200000);
        Lacze.Rysuj();
      }
      cout<<"Aktualna ilosc obiektow Wektor3D: " << Wektor3D::WezAktualnaIlosc() <<endl;
      cout<<"Laczna ilosc obiektow Wektor3D: " << Wektor3D::WezLacznaIlosc() <<endl;
      break;
    case 'm':
      cout<<"Twoj wybor, m - menu> m"<<endl;
      cout<<"r - zadaj ruch na wprost"<<endl;
      cout<<"o - zadaj zmiane orientacji"<<endl;
      cout<<"m - wyswietl menu"<<endl;
      cout<<"k - koniec dzialania programu"<<endl;
      cout<<"Aktualna ilosc obiektow Wektor3D: " << Wektor3D::WezAktualnaIlosc() <<endl;
      cout<<"Laczna ilosc obiektow Wektor3D: " << Wektor3D::WezLacznaIlosc() <<endl;
      break;
    case 'k':
      cout<<"Aktualna ilosc obiektow Wektor3D: " << Wektor3D::WezAktualnaIlosc() <<endl;
      cout<<"Laczna ilosc obiektow Wektor3D: " << Wektor3D::WezLacznaIlosc() <<endl;
      koniec=1;
      break;
    default:
      break;
    }
  }

  
}

/*Mikolaj Kurzawski  252894 Sterowanie dronem w obecnosci przeszkod 17.06.2020*/